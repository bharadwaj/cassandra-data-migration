package com.project.rbsk.migration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication
@ComponentScan("com.project.rbsk.migration")
@EnableJpaRepositories(basePackages = "com.project.rbsk.migration")
public class DatamigrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatamigrationApplication.class, args);
	}
}
