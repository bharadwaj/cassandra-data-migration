package com.project.rbsk.migration.controller;

import java.io.IOException;
import java.util.*;

import com.project.rbsk.migration.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.project.rbsk.migration.exception.ResourceNotFoundException;
import com.project.rbsk.migration.repository.LocationRepository;
import com.project.rbsk.migration.repository.SubmissionInfoRepository;
import com.project.rbsk.migration.repository.SubmissionInfoSqlRepository;
import com.project.rbsk.migration.repository.VistationPlanRepository;

@RestController
@RequestMapping("/")
public class SubmissionInfoController {

    @Autowired
    private SubmissionInfoRepository submissionInfoRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private SubmissionInfoSqlRepository submissionInfoSqlRepository;

    @Autowired
    private VistationPlanRepository vistationPlanRepository;

    @CrossOrigin
    @GetMapping("migration/{entryId}")
    public ResponseEntity checkcounters(@PathVariable String entryId) throws UnirestException, IOException {
        SubmissioninfoEntryResponse se = new SubmissioninfoEntryResponse();
        // Fetch submission Info by unique Id ie. Entry Id.
        SubmissionInfo foundSubmissionInfoRecord = submissionInfoRepository.findByEntryid(entryId);

        if (foundSubmissionInfoRecord == null)
            return new ResponseEntity<>(new MigrationResponse(), HttpStatus.NOT_FOUND);

        se.setBeneficarydata(foundSubmissionInfoRecord);

        //Get the MicroPlan Id of the submission Info.
        org.json.JSONObject obj = new org.json.JSONObject(foundSubmissionInfoRecord.getRequestbody());
        String microplanid = obj.optString("microPlanId");
        String mht_id = obj.optString("mht_id");

        //Get location by microplan Id
        Optional<VisitationPlan> visitation = vistationPlanRepository.findById(Integer.parseInt(microplanid));


        if (!visitation.isPresent())
            return new ResponseEntity<>(new MigrationResponse(), HttpStatus.BAD_REQUEST);

        se.setLocation(visitation.get().getLocation());

			/*Optional<SubmissionInfoSql> update = submissionInfoSqlRepository.findById(list1.getId());
					.orElseThrow(() -> new ResourceNotFoundException("SubmissionInfoSql", "NOT FOUND", list1.getId()));
			update.setFlag(1);
			submissionInfoSqlRepository.save(update);*/

        return new ResponseEntity<>(se, HttpStatus.OK);

    }
}
