package com.project.rbsk.migration.response;

import com.project.rbsk.migration.model.Location;
import com.project.rbsk.migration.model.SubmissionInfo;
import com.project.rbsk.migration.model.VisitationPlan;

public class SubmissionResponse {

    private Location location;
    private SubmissionInfo submissionInfo;
    private VisitationPlan visitationPlan;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public SubmissionInfo getSubmissionInfo() {
        return submissionInfo;
    }

    public void setSubmissionInfo(SubmissionInfo submissionInfo) {
        this.submissionInfo = submissionInfo;
    }

    public VisitationPlan getVisitationPlan() {
        return visitationPlan;
    }

    public void setVisitationPlan(VisitationPlan visitationPlan) {
        this.visitationPlan = visitationPlan;
    }
}
