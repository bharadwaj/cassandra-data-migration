package com.project.rbsk.migration.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="visitation_plan")
public class VisitationPlan  {
	/**Used to capture visitation scheduling. 
	 * This was referred to as Micro plan in discussions
	 */
	
	public static String ACTIVE = "ACTIVE"; 
	
	public static String CANCELLED = "CANCELLED"; 
	
	public static  String COMPLETED="COMPLETED";
	public static  String DAYSIGNOFF="DAYSIGNOFF";
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	
	@ManyToOne
	private Location location;
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	
	private String status;
	
	
	@NotNull
	private Date scheduledDate;


	public String getStatus() {
		return status;
	}

	public void setStatus(String oPEN2) {
		this.status = oPEN2;
	}


	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}
	
}
