package com.project.rbsk.migration.model;

public class MigrationResponse {

    private Location location;
    private SubmissionInfo beneficarydata;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public SubmissionInfo getBeneficarydata() {
        return beneficarydata;
    }

    public void setBeneficarydata(SubmissionInfo beneficarydata) {
        this.beneficarydata = beneficarydata;
    }
}

