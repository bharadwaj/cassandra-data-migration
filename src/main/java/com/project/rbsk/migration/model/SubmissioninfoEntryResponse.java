package com.project.rbsk.migration.model;

public class SubmissioninfoEntryResponse {
    private SubmissionInfo beneficarydata;
    private Location location;

    public SubmissionInfo getBeneficarydata() {
        return beneficarydata;
    }

    public void setBeneficarydata(SubmissionInfo beneficarydata) {
        this.beneficarydata = beneficarydata;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
