package com.project.rbsk.migration.model;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import com.datastax.driver.core.LocalDate;


@Table("submissioninfo")
public class SubmissionInfo {


    public static String PENDING = "PENDING";
    public static String WIP = "WIP";
    public static String ERROR = "ERROR";
    public static String COMPLETED = "COMPLETED";


    @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String entryid;

    @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.CLUSTERED)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDate creationdate;

    @PrimaryKeyColumn(ordinal = 1, type = PrimaryKeyType.CLUSTERED)
    private String createdby;

    private String requestbody;

    @PrimaryKeyColumn(ordinal = 2, type = PrimaryKeyType.CLUSTERED)
    private String status;

    private Date creationTime;


    private String completionStatus = PENDING;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getEntryid() {
        return entryid;
    }

    public void setEntryid(String entryid) {
        this.entryid = entryid;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getRequestbody() {
        return requestbody;
    }

    public void setRequestbody(String requestbody) {
        this.requestbody = requestbody;
    }


    public LocalDate getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(LocalDate creationdate) {
        this.creationdate = creationdate;
    }

    public Date getCreationTime() {
        return creationTime;
    }

//	public void setCreationTime(Timestamp timestamp) {
//		creationTime = timestamp;
//	}

    public void setCreationTime(Date date) {
        creationTime = date;
    }

    public String getCompletionStatus() {
        return completionStatus;
    }

    public void setCompletionStatus(String completionStatus) {
        this.completionStatus = completionStatus;
    }


}
