package com.project.rbsk.migration.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Location")
public class Location  {

	
	@Id
	@Column(name="id")
	private int id;
	
	private Date created;

	private Date lastModified;

	private String code;

	private String name;

	private String path;
	private int locationType_id;
	private String locationCode;
	private int country_code;
	private int state_code;
	private int zone_code;
	private int district_code;
	private int revenue_division_code;
	private int mandal_code;
	private int village_code;
	private String country_name;
	private String state_name;

	private String zone_name;
	private String district_name;
	private String revenue_division_name;
	private String mandal_name;
	private String village_name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getLocationType_id() {
		return locationType_id;
	}
	public void setLocationType_id(int locationType_id) {
		this.locationType_id = locationType_id;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public int getCountry_code() {
		return country_code;
	}
	public void setCountry_code(int country_code) {
		this.country_code = country_code;
	}
	public int getState_code() {
		return state_code;
	}
	public void setState_code(int state_code) {
		this.state_code = state_code;
	}
	public int getZone_code() {
		return zone_code;
	}
	public void setZone_code(int zone_code) {
		this.zone_code = zone_code;
	}
	public int getDistrict_code() {
		return district_code;
	}
	public void setDistrict_code(int district_code) {
		this.district_code = district_code;
	}
	public int getRevenue_division_code() {
		return revenue_division_code;
	}
	public void setRevenue_division_code(int revenue_division_code) {
		this.revenue_division_code = revenue_division_code;
	}
	public int getMandal_code() {
		return mandal_code;
	}
	public void setMandal_code(int mandal_code) {
		this.mandal_code = mandal_code;
	}
	public int getVillage_code() {
		return village_code;
	}
	public void setVillage_code(int village_code) {
		this.village_code = village_code;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getState_name() {
		return state_name;
	}
	public void setState_name(String state_name) {
		this.state_name = state_name;
	}
	public String getZone_name() {
		return zone_name;
	}
	public void setZone_name(String zone_name) {
		this.zone_name = zone_name;
	}
	public String getDistrict_name() {
		return district_name;
	}
	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}
	public String getRevenue_division_name() {
		return revenue_division_name;
	}
	public void setRevenue_division_name(String revenue_division_name) {
		this.revenue_division_name = revenue_division_name;
	}
	public String getMandal_name() {
		return mandal_name;
	}
	public void setMandal_name(String mandal_name) {
		this.mandal_name = mandal_name;
	}
	public String getVillage_name() {
		return village_name;
	}
	public void setVillage_name(String village_name) {
		this.village_name = village_name;
	}
	@Override
	public String toString() {
		return "Location [id=" + id + ", created=" + created + ", lastModified=" + lastModified + ", code=" + code
				+ ", name=" + name + ", path=" + path + ", locationType_id=" + locationType_id + ", locationCode="
				+ locationCode + ", country_code=" + country_code + ", state_code=" + state_code + ", zone_code="
				+ zone_code + ", district_code=" + district_code + ", revenue_division_code=" + revenue_division_code
				+ ", mandal_code=" + mandal_code + ", village_code=" + village_code + ", country_name=" + country_name
				+ ", state_name=" + state_name + ", zone_name=" + zone_name + ", district_name=" + district_name
				+ ", revenue_division_name=" + revenue_division_name + ", mandal_name=" + mandal_name
				+ ", village_name=" + village_name + "]";
	}
	
	
	
	

}