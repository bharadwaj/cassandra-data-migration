package com.project.rbsk.migration.repository;

import com.project.rbsk.migration.model.VisitationPlan;
import org.springframework.data.repository.CrudRepository;

public interface VistationPlanRepository extends CrudRepository<VisitationPlan, Integer> {

}
