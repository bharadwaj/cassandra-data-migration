package com.project.rbsk.migration.repository;

import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.project.rbsk.migration.model.SubmissionInfo;

@Repository
public interface SubmissionInfoRepository  extends CassandraRepository<SubmissionInfo,String> {
	SubmissionInfo findByEntryid(String string);
}
