package com.project.rbsk.migration.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.rbsk.migration.model.Location;

public interface LocationRepository extends JpaRepository<Location, Integer>{

}
